# create-react

## Build docker image and push it to the dockerhub

    docker build -t neatsoft/create-react .
    docker login
    docker push neatsoft/create-react

## Create new project

    docker run -it -u `id -u`:`id -g` -v `pwd`:/app neatsoft/create-react <project_name>
