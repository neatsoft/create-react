FROM node:10-slim

WORKDIR /app

COPY ./entrypoint /

ENTRYPOINT ["/entrypoint"]
